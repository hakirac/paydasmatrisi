import React, { Component } from 'react'
import Hucre from './hucre'
import ornekVeri from './demo_data'
import './tablo.css'

class Tablo extends Component {

    tBody = () => {
        return ornekVeri.kisiler.map(x => {
            return (
                <tbody>
                    <tr>
                        <td>{x.isim}</td>
                        {
                            ornekVeri.projeler.map((y, i) => {
                                return (<td>
                                    {
                                        x.projeler.map(z => {
                                            if(z.projeId == y.id) {
                                               return <Hucre deger="PY" mPozisyon={z.pozisyonu}
                                                pRolu={z.projedekiRolu} paydasBelgesi={z.paydasBelgesi} />
                                            } else return ""
                                        })

                                    }
                                </td>)
                            })

                        }
                    </tr>

                </tbody>
            )
        })
    }

    tHead = () => {
        return (
            <thead>
                <tr>
                    <th>

                    </th>
                    {ornekVeri.projeler.map(a => <th><div><span>{a.proje}</span></div></th>)}

                </tr>
            </thead>
        )
    }

    render() {
        return (<div>
            <table style={{marginTop:"50px"}}>
                {this.tHead()}
                {this.tBody()}

            </table>
        </div>
        )
    }

}

export default Tablo
