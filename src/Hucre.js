/*
* @autor: Hakan Özdaşçı, 03.10.2018
* Bu component kendisine gönderilen "x" parametresinin 
*/

import React, { Component } from 'react';

// @param: deger / ornek: X veya PY 
// @param: mPozisyon / ornek: müdür yardımcısı
// @param: pRolu / ornek: proje yöneticisi
// @param: paydasBelgesi / ornek: http://www.cbsohzdasci/paydasbelgeler 

class Hucre extends Component {

  constructor(props) {
    super()
    this.state = {
      isActive: true
    }
    this.handleHoverOn = this.handleHoverOn.bind(this);
    this.handleHoverOff = this.handleHoverOff.bind(this);
  }

  handleHoverOn() {
    this.setState(prev => ({ isActive: false }))
  }

  handleHoverOff() {
    this.setState(prev => ({ isActive: true }))
  }

  render() {

    var stil = {
      display: this.state.isActive ? "none" : "block",
      position: "absolute",
      top: "-60px",
      backgroundColor: "black",
      color: "white",
      zIndex: "3",
      width:"500px",
      padding:"10px",
      textAlign:"left"
      
    }

    return (
      <a href={this.props.paydasBelgesi} onMouseEnter={this.handleHoverOn} onMouseLeave={this.handleHoverOff}
        style={{
          position: "relative" 
        }}>
        
        {this.props.deger}
        
      <div style={stil}>
        <label>Proje Adı: </label> <span>{this.props.projeAdi}</span> <br />
          <label>Pozisyon: </label> <span>{this.props.mPozisyon}</span> <br />
          <label>Projedeki Rolü: </label> <span>{this.props.pRolu}</span>
          
        </div>
      </a>
    )
  }

}


export default Hucre