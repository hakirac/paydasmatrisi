import React, { Component } from 'react'
import Hucre from './Hucre'
// veri girdi yapisini gormek isterseniz demo_data.js dosyasini inceleyiniz.
//import demo_data from './Tablo.js_demo_data'
import './tablo.css'
import './prototypeGroupBy.js'
import ActiveButton from './ActiveButton'
import aktifLink from './aktifLink'

// import axios from 'axios'

class Tablo extends Component {

    constructor(props) {
        super()
        this.state = {

            veri: {},
            isLoading: true, 
            degisiklik:false,
            error: null,
            birim:"AŞM", // varsayilan olarak bu mudurlugun verisi cekilecek
            projeDurumu:"tumu" // ilgili mudurlukle ilgili tum veriler cekilecek
        }
        this.handleClick = this.handleClick.bind(this);
        this.handleClickBirim = this.handleClickBirim.bind(this);
        this.handleClickProjeDurumu = this.handleClickProjeDurumu.bind(this);
    }

    // proje yoneticileri veya isimlere tiklayinca
    handleClick(event) {
        event.preventDefault()
        let path = event.target.pathname    
        path = path.substring(1, path.length - 1).split(",")      

        // tiklanan linkteki idleri alip, birimler icerisindeki idler ile kiyaslayip
        // bulunan idler belirli bir array'e atiliyor ve eslesmeyenler de baska bir arraye atiliyor
        // sonra iki array birlestirilip, state guncelleniyor.
        let arrVeriEslesen = []
        let arrVeriUymayan = []    
        
        let birimler = this.state.veri.birimler                
        
        Object.keys(birimler).forEach(b=>{
            birimler[b].forEach(c=>{
                    path.includes(c.ID.toString()) ? arrVeriEslesen.push(c) : arrVeriUymayan.push(c)
            })            
        })

        let birimler2 = arrVeriEslesen.concat(arrVeriUymayan).groupBy(x=>x.birim) // gruplayarak birimleri tekrar key olarak aliyoruz.
        let veri2 = this.state.veri
        veri2.birimler = birimler2
        console.log("bilgi"+veri2.birimler)
        this.setState({veri:veri2})

    }

    handleClickBirim(event) {
        event.preventDefault()
        let birim1 = event.target.getAttribute("href")
        birim1 = birim1.substring(1, birim1.length)
        
        this.setState({birim:birim1, degisiklik:true})
        aktifLink("birimBasligi", event, "secili")

    }

    // proje durumlari : devam eden, tumu, iptal edilen, tamamlanan vs.
    handleClickProjeDurumu(event) {
        event.preventDefault()
        let projeDurumu1 = event.target.getAttribute("href")
        projeDurumu1 = projeDurumu1.substring(1, projeDurumu1.length)
        this.setState({projeDurumu:projeDurumu1, degisiklik:true})

         let y = document.getElementById("kategoriler").getElementsByTagName("a")
    for(let el = 0; el<y.length; el++) {
        if (y[el].id.toString() == event.target.id.toString()) y[el].className += " secili"; else y[el].classList.remove("secili")
    }
        
    }        

    // her bir kisi icin yapilan islem
    Satir = (kisi, birimler, json, ilkDongu) => {
        let snc = [];
        let kisiIDs = "" // eslesen idleri kisi isimlerindeki linke eklemek icin
        let imlemeler;
        
        // birim isimleri 
        if (ilkDongu) {
            snc.push(this.tdSolDikeySutun(json)) 
            ilkDongu = false
        }        
        
        // kisiye ait bilgiler baz alinarak birimlerdeki projelerin id'leri ile kiyaslama yapılıyor ve   satir icin yeni hucre olusturuyor 
        imlemeler = Object.keys(birimler).map(birim => (

                birimler[birim].map(bp => {
                    let durum;
                    durum = kisi.map(kp => {

                        if (bp.ID == kp.projeID) {
                            kisiIDs += kp.projeID +"," // eslesen idleri kisi isimlerindeki linke eklemek icin 
                            let dege = kp.projedekiRolu == "Proje Yöneticisi" ? "PY" : "X"
                            return <Hucre deger={dege} mPozisyon={kp.pozisyonu} pRolu={kp.projedekiRolu} projeAdi = {bp.projeAdi} paydasBelgesi="" />
                            
                        }
                        else
                            return false
                    })

                    if (durum) return <td>{durum} </td>
                    else return <td></td>
                })
            ))

            snc.push(<td className="isimler"><a href={"/"+kisiIDs} onClick={this.handleClick}>{kisi[0].isim}</a></td>)
            snc.push(<td className="toplamProjeSayisi">{kisiIDs.slice(0, kisiIDs.length-1).split(",").length}</td>)
            snc.push(imlemeler)
        
        return snc
    }

    tBody = (json) => {
        let ilkDongu = true
        return <tbody>
            {Object.keys(json.kisiler).map(kisi => <tr>                
                {this.Satir(json.kisiler[kisi], json.birimler, json, ilkDongu)}
                {ilkDongu = false}
            </tr>)}

        </tbody>
    }

    /*tBody222 = () => {
        return ornekVeri.kisiler.map(x => {
            return (
                <tbody>
                    <tr>
                        <td>{x.isim}</td>
                        {
                            ornekVeri.projeler.map((y, i) => {
                                return (<td>
                                    {
                                        x.projeler.map(z => {
                                            if(z.projeId == y.id) {
                                               return <Hucre deger="PY" mPozisyon={z.pozisyonu}
                                                pRolu={z.projedekiRolu} paydasBelgesi={z.paydasBelgesi} />
                                            } else return ""
                                        })

                                    }
                                </td>)
                            })

                        }
                    </tr>

                </tbody>
            )
        })
    }*/

    tHead = (json) => {

        return (
            <thead className="projeler">
                <tr>
                    <th></th> <th></th> 
                    <th><div style={{transform:"translate(5px, 149px) rotate(-90deg)"}}><span style={{padding:"15px 10px", width:"108px"}}>TOPLAM ROL</span></div></th> 
                    {                        
                        Object.keys(json).map(a => (

                            json[a].map(i =>
                                <th>
                                    <div><span>{i.projeAdi}</span></div>
                                </th>
                            )

                        ))
                    }

                </tr>
            </thead>
        )
    }


    // birim isimlerini sol tarafta dikey olarak olusturuyoruz.
     tdSolDikeySutun = (json) => {
        return <td rowSpan={Object.keys(json.kisiler).length} className="birimBasligiSatir">
            <div id="birimBasligi" className="birimBasligi">
                <ul >                
                    {json.birimIsimleri.map((x,it)=> <li> <a id={"a"+it} href={"/"+x} className={x == this.state.birim ? " secili": ""} onClick={this.handleClickBirim}> {x} </a> </li> )}
                </ul>  
            </div>
        </td>
    } 

    /*// birimlerin ustte olmasi hedeflenmisti ama iptal edildi bu fonksiyon.
    tHead1 = (json) => {

        return (
            <thead className="birimler">
                <tr>
                    <td></td> 
                    {
                        Object.keys(json).map(a => <td colSpan={json[a].length}>
                            <center><div><span>{a}</span></div></center>
                        </td>
                        )}
                </tr>
            </thead>
        )
    }*/

    veriCek() {

        let birim = this.state.birim
        let projeDurumu = this.state.projeDurumu
        // let path = birim+"/"+projeDurumu

        fetch('http://cbshozdasci/bidb/matris/'+birim+"/"+projeDurumu)
            .then(response => {
                if (response.ok) {
                    return response.json()
                } else {
                    throw new Error('Something went wrong ...');
                }
            })
            .then(data => this.setState({ veri: data, isLoading: false, degisiklik:false }))
            .catch(error => this.setState({ error, isLoading: false, degisiklik:false }));
    }

  
    componentDidMount() {
        this.veriCek()
    }

    render() {

        if(this.state.degisiklik) this.veriCek()

        const { veri, isLoading, error } = this.state;

        if (error) {
            return <p>{error.message}</p>;
        }
        if (isLoading) {
            return <center><p>Loading ...</p></center>;
        }
        /* {this.tHead1(rsp.birimler)}*/
        let rsp = veri
        //  console.log("fetch: "+ JSON.stringify(rsp))
        return (<div>
            <ActiveButton action={this.handleClickProjeDurumu}/>
            <table style={{ marginTop: "-20px", borderSpacing: "0px", borderCollapse: "separate" }}>
                
                {this.tHead(rsp.birimler)}
                {this.tBody(rsp)}

            </table>
        </div>
        )
    }
}

export default Tablo