export default {

    "kisiler": {
        "Kazim Karabekir": [
            { "isim": "Kazim Karabekir", "paydasBelgesi": "\\\\nasserver\\02_Bilgi_Teknolojileri_DB\\01_BTDB_Ortak\\PMO\\Projeler/BİM/Kurum İçi Yazılım Şefliği/Sosyal Hizmet Uygulamaları/PBS (Protokol Bilgilendirme Sistemi)_Yaz Gel_ÖKM_v1.0_Insource_Hasan Sencer KIRTIL_Proje Paydaş Belgesi.docx", "projeID": 189, "pozisyonu": "Müdür Yardımcısı", "projedekiRolu": "Müdür Yardımcısı" },
            { "isim": "Kazim Karabekir", "paydasBelgesi": "\\\\nasserver\\02_Bilgi_Teknolojileri_DB\\01_BTDB_Ortak\\PMO\\Projeler/BİM/Dış Yazılım Şefliği/Web Gurubu/Şehir Tiyatroları Faz 3_Yaz Gel_BIM_w1.0_Outsource_Elif KÜÇÜKHÜSEYİN_Proje Paydaş Belgesi.docx", "projeID": 51, "pozisyonu": "-", "projedekiRolu": "-" },
            { "isim": "Kazim Karabekir", "paydasBelgesi": "\\\\nasserver\\02_Bilgi_Teknolojileri_DB\\01_BTDB_Ortak\\PMO\\Projeler/BİM/Dış Yazılım Şefliği/Web Gurubu/Şehir Tiyatroları Faz 2_Yaz Gel_BIM_w1.0_Outsource_Elif KÜÇÜKHÜSEYİN_Proje Paydaş Belgesi.docx", "projeID": 40, "pozisyonu": "", "projedekiRolu": "" },
            { "isim": "Kazim Karabekir", "paydasBelgesi": "\\\\nasserver\\02_Bilgi_Teknolojileri_DB\\01_BTDB_Ortak\\PMO\\Projeler/BİM/Kurum İçi Yazılım Şefliği/Sosyal Hizmet Uygulamaları/UTK Takip Sistemi_Yaz Gel_UKM_v1.0_Insource_Muhammet ÜNLÜ_Proje Paydaş Belgesi.docx", "projeID": 13, "pozisyonu": "Ekip Lideri", "projedekiRolu": "Ekip Lideri" },
            { "isim": "Kazim Karabekir", "paydasBelgesi": "\\\\nasserver\\02_Bilgi_Teknolojileri_DB\\01_BTDB_Ortak\\PMO\\Projeler/PMO/İBB 2019 IT Taleplerinin Toplanması_Genel_BİDB_v1.0_Insource_Veysel BEYTUR_Proje Paydaş Belgesi.docx", "projeID": 15, "pozisyonu": "", "projedekiRolu": "" },
            { "isim": "Kazim Karabekir", "paydasBelgesi": "\\\\nasserver\\02_Bilgi_Teknolojileri_DB\\01_BTDB_Ortak\\PMO\\Projeler/BİM/Kurum İçi Yazılım Şefliği/Sosyal Hizmet Uygulamaları/YİMTA (İmalat Takip Sistemi)_Yaz Gel_YBM_v1.0_Insource_Hasan Sencer KIRTIL_Proje Paydaş Belgesi.docx", "projeID": 73, "pozisyonu": "Ekip Lideri", "projedekiRolu": "Ekip Lideri" }
        ],
        "#2": [
            { "isim": "#2", "paydasBelgesi": "\\\\nasserver\\02_Bilgi_Teknolojileri_DB\\01_BTDB_Ortak\\PMO\\Projeler/BİM/Kurum İçi Yazılım Şefliği/Sosyal Hizmet Uygulamaları/UTK Takip Sistemi_Yaz Gel_UKM_v1.0_Insource_Muhammet ÜNLÜ_Proje Paydaş Belgesi.docx", "projeID": 72, "pozisyonu": "Yol ve Bakım Müdürlüğü", "projedekiRolu": "…….." },
            { "isim": "#2", "paydasBelgesi": "\\\\nasserver\\02_Bilgi_Teknolojileri_DB\\01_BTDB_Ortak\\PMO\\Projeler/BİM/Kurum İçi Yazılım Şefliği/Sosyal Hizmet Uygulamaları/YİMTA (İmalat Takip Sistemi)_Yaz Gel_YBM_v1.0_Insource_Hasan Sencer KIRTIL_Proje Paydaş Belgesi.docx", "projeID": 73, "pozisyonu": "Yol Bakım Müdürlüğü,", "projedekiRolu": "…….." }
        ], "3B Şefliği": [{ "isim": "3B Şefliği", "paydasBelgesi": "\\\\nasserver\\02_Bilgi_Teknolojileri_DB\\01_BTDB_Ortak\\PMO\\Projeler/CBS/Yazılım Geliştirme Şefliği/Visionist_Yaz Gel_AŞM_w1.0_Insource_Adem YILDIZ_Proje Paydaş Belgesi.docx", "projeID": 167, "pozisyonu": "", "projedekiRolu": "Veri Sağlayıcı" }
        ],
        "Abdulkadir İNCEOĞLU": [
            { "isim": "Abdulkadir İNCEOĞLU", "paydasBelgesi": "\\\\nasserver\\02_Bilgi_Teknolojileri_DB\\01_BTDB_Ortak\\PMO\\Projeler/BİM/Kurum İçi Yazılım Şefliği/Bütçe ve Denetim/Bütçe Modülü Faz2_Yaz Gel_BDM_w2.0_Insource_Esme Gül TOPRAK_Proje Paydaş Belgesi.docx", "projeID": 186, "pozisyonu": "Bütçe ve Denetim Müdürlüğü, Müdür", "projedekiRolu": "Proje Kabul Onay Yetkilisi" }
        ],
        "Abdulkadir İnceoğlu": [
            { "isim": "Abdulkadir İnceoğlu", "paydasBelgesi": "\\\\nasserver\\02_Bilgi_Teknolojileri_DB\\01_BTDB_Ortak\\PMO\\Projeler/BİM/CRM/Siebel Hukuk Uygulaması & HYS Entegrasyonu_Yaz Gel_MHM_v1.0_Insource_Erkan KARAGÜLMEZ_Proje Paydaş Belgesi.docx", "projeID": 198, "pozisyonu": "Müdür", "projedekiRolu": "Müşteri Temsilcisi" },
            { "isim": "Abdulkadir İnceoğlu", "paydasBelgesi": "\\\\nasserver\\02_Bilgi_Teknolojileri_DB\\01_BTDB_Ortak\\PMO\\Projeler/BİM/Kurum İçi Yazılım Şefliği/Muhasebe/Muhasebe İyileştirme_Yaz Gel_MHDB_w1.0_Insource_Tuba YILMAZ_Proje Paydaş Belgesi.docx", "projeID": 64, "pozisyonu": "Bütçe ve Denetim Müdürü", "projedekiRolu": "Müşteri" }
        ],
        "Abdullah Er": [
            { "isim": "Abdullah Er", "paydasBelgesi": "\\\\nasserver\\02_Bilgi_Teknolojileri_DB\\01_BTDB_Ortak\\PMO\\Projeler/CBS/Yazılım Geliştirme Şefliği/Arıza Yönetimi Uygulaması_Yaz Gel_ESM_w1.0_Insource_Pervin İŞCAN_Proje Paydaş Belgesi.docx", "projeID": 170, "pozisyonu": "Proje Paydaşı", "projedekiRolu": "Analist/Yazılım Geliştirme" }
        ]
    },
    "birimler":{
        "ESM":[
            {"ID":189,"birim":"ESM","projeAdi":"153 Çağrı Merkezi Bakım Onarım ve Malzeme Alımı"},
            {"ID":186,"birim":"ESM","projeAdi":"APTİLO (İBB WIFI Hotspot Sistemi)"},
            {"ID":170,"birim":"ESM","projeAdi":"Afete duyarlı kent hasar takip ve izleme sistemi"},
            {"ID":136,"birim":"ESM","projeAdi":"Alcatel Telefon ve Telefon Santralleri"},
            {"ID":137,"birim":"ESM","projeAdi":"Alcatel Çağrı Merkezi Sistemi"},
            {"ID":115,"birim":"ESM","projeAdi":"Altyapı Geçiş Hakkı Uygulaması"}
        ],
        "BİM": [
            {"ID":40,"birim":"BİM","projeAdi":"LDAP API"},
            {"ID":13,"birim":"BİM","projeAdi":"LIMS (Çevre Laboratuvarı Bilgi Yönetimi)"},
            {"ID":15,"birim":"BİM","projeAdi":"MEBİS (Mezarlık Otomasyonu)"},
            {"ID":14,"birim":"BİM","projeAdi":"MEBİS (Mezarlık Otomasyonu)"},
            {"ID":85,"birim":"BİM","projeAdi":"Masaüstü Sanallaştırma"},
            {"ID":41,"birim":"BİM","projeAdi":"Metro Haritası Modülü"}
        ],
        "CBS": [
            {"ID":87,"birim":"CBS","projeAdi":"2.5B Perspektif Kent Modeli"},
            {"ID":88,"birim":"CBS","projeAdi":"2017 Yılı Uydu Görüntüsü"},
            {"ID":89,"birim":"CBS","projeAdi":"3B Kent Modeli 2017"},
            {"ID":231,"birim":"CBS","projeAdi":"3B İstanbul"},
            {"ID":232,"birim":"CBS","projeAdi":"Akıllı Şehir Dashboard"},
            {"ID":159,"birim":"CBS","projeAdi":"Altyapı Bilgi Sistemi"}            
        ]
    },
    "birimIsimleri": [
        "ESM", "BİM", "CBS", "ITSM", "AŞM"
    ]
}