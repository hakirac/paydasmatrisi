import React, { Component } from 'react'
import './activeButton.css'

class ActiveButton extends Component {
constructor(props) {
    super()
    this.state = {active:false}

}

    componentDidMount() {         

       // this.setState ({active:true})
        // event.target.className += " active"
        var header = document.getElementById("kategoriler");
        var links = header.getElementsByTagName("a");
        var urlPathname = window.location.pathname;

        for (var i = 0; i < links.length; i++) {
            if (links[i].pathname == urlPathname) {
                links[i].className += " active";
            }
        }
    }

    render() {
        return (
            <div id="kategoriler" ref="kategoriler">
                <a id="a1" href="/tumu" className="secili" onClick={this.props.action}> Tümü </a>
                <a id="a2" href="/devameden" onClick={this.props.action}> Devam Edenler </a>
                <a id="a3" href="/gecti" onClick={this.props.action}> Geçti </a>
                <a id="a4" href="/donduruldu" onClick={this.props.action}> Donduruldu </a>
                <a id="a5" href="/iptal" onClick={this.props.action}> İptal </a>
                <a id="a6" href="/tamamlananlar" onClick={this.props.action}> Tamamlananlar </a>
            </div>
        )
    }
}






export default ActiveButton