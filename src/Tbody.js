import React, { Component } from 'react'
import demo_data from './demo_data'
import Hucre from './hucre';


const Row = (props) => {

    return demo_data.kisiler.map(x => {
        let rol = ""
        let arr = []
        arr.push(<tr>)
        arr.push(<td> {x.isim} </td>)

        return demo_data.projeler.map(y=>{
                rol = x.projedekiRolu == "Proje Yöneticisi" ? "PY" : "X"
            arr.push(
                <td>
                <Hucre deger={rol} durumID={y.id} mPozisyon={x.pozisyonu} pRolu={x.projedekiRolu} paydasBelgesi={x.paydasBelgesi} />
            </td>
            )
            return arr
        })
        arr.push(</tr>) 
    })


}
    



class Tbody extends Component {

    constructor(props) {
        super()
    }

    render() {
        return (
            <table>
                <tbody>
                    <Row />
                </tbody>
            </table>)
    }
}

export default Tbody