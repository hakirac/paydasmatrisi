function activeButton () {

    var header = document.getElementById("kategoriler");
    var links = header.getElementsByTagName("a");
    var urlPathname = window.location.pathname;

    for(var i = 0; i<links.length; i++) {
        if( links[i].pathname == urlPathname ) {
         links[i].className += " active";      
        }
    }
}

window.onload = activeButton
    

/*
// Add active class to the current button (highlight it)
var header = document.getElementById("kategoriler1");
var btns = header.getElementsByClassName("linkler");
for (var i = 0; i < btns.length; i++) {

  btns[i].addEventListener("click", function(event) {
      event.preventDefault();      
    var current = header.getElementsByClassName("active");
    
    current[0].className = current[0].className.replace(" active", "");
    this.className += " active";    
  });
}
*/